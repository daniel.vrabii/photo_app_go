package main
import (
	"fmt"
	"net/http"
)

//when someone enters the web app
func handlerFunc(w http.ResponseWriter, r *http.Request) {
fmt.Fprint(w, "<h1>Welcome!</h1>")
}

func main() {
	http.HandleFunc("/", handlerFunc)
	http.ListenAndServe(":3000", nil)
}